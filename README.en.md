# 低价寄快递寄件微信小程序 实际商用版，对接了低价快递渠道，运营平台赚取差价，支持市面上全部主流快递

#### Description
2023年蓝海项目，低价寄快递小程序前端模板，实际商用版，对接了低价快递渠道，运营平台赚取差价，支持市面上全部主流快递。对接低价渠道 支持微信支付 补差价 在线客服 。前台使用的uniapp 后台使用的java


#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
